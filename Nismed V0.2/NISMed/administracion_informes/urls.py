#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.conf.urls import url

from . import views


urlpatterns = [
# Template que muestra la lista de espermiogramas creados, mas adelante cambia por: listar_examenes
#url(r'^listar_espermio/', views.listar_espermio, name='listar_espermio'),

url(r'^informe_turno/', views.informeTurno, name='informe_turno'),

]
