
#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from administracion_turnos.models import *
from administracion_turnos.views import *
from administracion_usuarios.models import *
from administracion_usuarios.views import *
from django.contrib.auth.models import Permission, auth



def logout(request):
	auth.logout(request)
	return redirect('login')




def cuenta(request):
	user = request.user
	if (user.has_perm('User.Ver_turno') == False and user.has_perm('User.Ver_sala') == False  and user.has_perm('User.Ver_reperfusion') == False ):
		usuario = medico.objects.get(username=request.user)
		permission1 = Permission.objects.get(codename = 'Ver_turno')
		permission2 = Permission.objects.get(codename = 'Ver_sala')
		permission3 = Permission.objects.get(codename = 'Ver_reperfusion')





		if (usuario.tipo == 'tur-bec'):

		 	user.user_permissions.add(permission1)

			return redirect('home_turno')

		elif (usuario.tipo == 'tur-staff'):

		 	user.user_permissions.add(permission1)

			return redirect('home_turno')

		elif (usuario.tipo == 'sala-staff'):

		 	user.user_permissions.add(permission2)

		elif (usuario.tipo == 'reper-staff'):

		 	user.user_permissions.add(permission3)

		else :

		 	user.user_permissions.add(permission3)



	return redirect('home_reperfusion')
