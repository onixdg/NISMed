"""NISMed URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from . import views
#from django.contrib.auth.views import login, password_reset, password_reset_done, password_reset_confirm, password_reset_complete
from django.contrib.auth import views as auth_views



urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^administracion_turnos/', include('administracion_turnos.urls')),
    url(r'^administracion_pacientes/', include('administracion_pacientes.urls')),
    url(r'^administracion_usuarios/', include('administracion_usuarios.urls')),
    url(r'^administracion_informes/', include('administracion_informes.urls')),
    url(r'^administracion_reperfusion/', include('administracion_reperfusion.urls')),

    url(r'^$', auth_views.login, {'template_name':'login.html'}, name = 'login'),
    url(r'^logout/$', auth_views.logout, {'next_page':'login'}, name = 'logout'),
     url(r'^cuenta/', views.cuenta, name='cuenta'),





]

if settings.DEBUG:
    urlpatterns+= static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
