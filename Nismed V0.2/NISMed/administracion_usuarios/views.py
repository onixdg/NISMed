# -*- coding: utf-8 -*-
from __future__ import unicode_literals

#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
#from django.contrib.auth.forms import UserCreationForm
from models import *
from django.core.urlresolvers import reverse_lazy
from forms import *
from django.contrib.auth.models import Permission


def registrar(request):

	 	if request.method =='POST':
	 		form = medicoForm(request.POST)

	 		if form.is_valid():
	 			passw = form.cleaned_data.get('password')
	 			obj = form.save(commit=False)
	 			obj.set_password(passw)
	 			obj.save()
	 			return redirect('login')

	 	else:

	 		form= medicoForm()
	 	return render (request, 'usuario/registrar.html',{'form':form})
