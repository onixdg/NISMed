# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib import admin

from django.db import models


#!/usr/bin/env python # -*- coding: utf-8 -*- from django.contrib.auth.models import User from django.contrib.auth.forms import UserCreationForm from django.contrib import admin from django.db import models class User_detail(User) : 	ACCOUNT_TYPE = ( 		(1,'Responsible for the system'), 		(2,'Responsible for the study'), 		(3,'Reader') 		) 	Account_type = models.IntegerField(choices= ACCOUNT_TYPE, default = 3) 	activation_key = models.CharField(max_length=40, blank=True) 	key_expires = models.DateTimeField(auto_now_add=True) 		def __str__(self): 		return self.username 	class Meta: 		permissions = ( 					('Read_database_use_data','Read the data associated with the use of the database'), 			('Read_database_metadata','Read the metadata of the database'), 			('Read_user_data','Read user identification data'), 			('Read_study_public_data','Read study public data '), 			('Read_study_private_data','Read study private data'), 			('Edit_user_metadata','Edit the metadata of the user'), 			('Create_data','Create data'), 			('Edit_study_private_data', 'Edit the metadata of the database'), 							) 	admin.site.register(User_detail)


class medico(User) :
	TIPO = (
		('tur-bec' , 'Turno: Becado'),
		('tur-staff' , 'Turno:  Staff'),
		('sala-bec' , 'Sala: Becado'),
		('sala-staff' , 'Sala: Staff'),
		('reper-staff' , 'Reperfusión: Staff'),
		#('trom-bec' , 'OWN ACQUISITION'),
		#('trom-staff' , 'OTHER ACQUISITION'),

		)

	tipo = models.CharField(max_length =20,choices= TIPO)
	#activation_key = models.CharField(max_length=40, blank=True)
	#key_expires = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.username

	class Meta:
		permissions = (

			('Ver_turno','Ver contenido de Módulo Administración de Turnos'),
			('Ver_sala','Ver contenido de Módulo Administración de Sala'),
			('Ver_reperfusion','Ver contenido de Módulo Administración de Terapia de Reperfusión'),





		)
