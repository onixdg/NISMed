from django import forms
from .models import *
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
#from captcha.fields import ReCaptchaField
#from django.contrib.auth.hashers import check_password



class medicoForm (forms.ModelForm ):

	#captcha = ReCaptchaField()
	username = forms.CharField(max_length=30)
	password =  forms.CharField(widget=forms.PasswordInput())
	confirm_password= forms.CharField(widget=forms.PasswordInput())


	class Meta:
		model =medico
		fields = ['password', 'username', 'first_name', 'last_name','email','tipo']


	def clean_first_name(self):
		diccionario_limpio = self.cleaned_data
		first_name = diccionario_limpio.get("first_name")
		if (first_name == None or (len(first_name))==0) :
			raise forms.ValidationError("This field is required.")
		return first_name


	def clean_last_name(self):
		diccionario_limpio = self.cleaned_data
		last_name = diccionario_limpio.get("last_name")
		if (last_name == None or (len(last_name))==0) :
			raise forms.ValidationError("This field is required.")
		return last_name


	def clean_tipo(self):
		diccionario_limpio = self.cleaned_data
		tipo = diccionario_limpio.get("tipo")
		if (tipo == None) :
			raise forms.ValidationError("This field is required.")
		return tipo

	def clean_confirm_password(self):

		diccionario_limpio = self.cleaned_data
		password = diccionario_limpio.get('password')
		confirm_password = diccionario_limpio.get('confirm_password')
		if (confirm_password == None or (len(confirm_password))==0) :
			raise forms.ValidationError("This field is required.")


		if password != confirm_password:
			raise forms.ValidationError("Password does not match.")

		return confirm_password

	#def clean_email(self):
		#diccionario_limpio = self.cleaned_data
		#email = diccionario_limpio.get("email")

		#if (email == None or (len(email))==0) :

#				raise forms.ValidationError("This field is required.")


#		else:
#			validacion = medico.objects.filter(email = email)
#			if self.instance.pk is not None:
#				if self.instance.email != email:
#					if (validacion) :
#							raise forms.ValidationError("The email already exists in the system." )
#			else:
#							raise forms.ValidationError("The email already exists in the system."  )
#
#		return email
