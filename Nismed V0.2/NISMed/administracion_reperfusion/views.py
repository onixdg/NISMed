# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from administracion_reperfusion.models import *
from administracion_usuarios.models import medico
from administracion_pacientes.models import *
from django.http import HttpResponse
from forms import *
from administracion_pacientes.forms import *
from django.shortcuts import get_object_or_404
from datetime import *
from datetime import date
from datetime import timedelta
from administracion_informes.views import *
from django.utils import timezone

# Create your views here.


def homeReperfusion (request):


	Reperfusion = reperfusion.objects.all().order_by('fecha')



	#Control = control.objects.filter(hospitalizacion= Hospitalizacion)
	#ControlI = Control[1]Turno
	#ControlF = Control[-1]
	#fI = ControlI.fecha
	hoy = date.today()
	#dias =  (fF - fI).days
	ahora = datetime.now()

	User= request.user.username

	Medico = medico.objects.filter(username = User)







	return render (request, 'reperfusion/home_reperfusion.html', {'Reperfusion':Reperfusion , 'hoy': hoy, 'ahora': ahora, 'Medico':medico})



#REPERFUSION-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------CREAR Y CERRAR TURNO

def crearReperfusion (request):
 	#if (paciente_id == None):
 		#return redirect('seleccionar_paciente')
 	#else:
	if request.method =='POST':

		form_p = pacienteRapForm(request.POST)  # ojoo con los pacientes rapidos
		form_r = reperfusionForm(request.POST)
		form_t = tiempoForm(request.POST)
		form_a = antecedentesForm(request.POST)
		form_s = scoreForm(request.POST)
		if  form_p.is_valid() and form_r.is_valid():
			form_p.save()
			Paciente = paciente.objects.last()
			Evento = evento.objects.create(fecha = date.today() , origen = 'reperfusion', tipo = 'ingreso', paciente = Paciente)



			form_r.save()
			form_t.save()
			form_a.save()
			form_s.save()
			Tiempo = tiempo.objects.last()
			Antecedentes = antecedentes.objects.last()
			Score = score.objects.last()

			Reperfusion = reperfusion.objects.last()
			Reperfusion.evento = Evento
			Tiempo.reperfusion = Reperfusion
			Antecedentes.reperfusion = Reperfusion
			Score.reperfusion = Reperfusion



			Reperfusion.save()
			Tiempo.save()
			Antecedentes.save()
			Score.save()




	 			#messages.add_message(request, messages.SUCCESS, ('La hora de %(tipo)s para %(paciente)s se ha generado de forma exitosa.') % {'tipo':tipo, 'paciente':paciente})

			return redirect('home_reperfusion')
	else:

		form_p = pacienteRapForm()
		form_r = reperfusionForm()
		form_t = tiempoForm()
		form_a = antecedentesForm()
		form_s = scoreForm()



	form_r.fields['fecha'].initial = date.today()


	return render (request, 'reperfusion/crear_reperfusion.html',{'form_p':form_p, 'form_r':form_r, 'form_t':form_t, 'form_a':form_a, 'form_s':form_s})




def editarReperfusion (request, reperfusion_id = None):
	Reperfusion = get_object_or_404(reperfusion,pk =reperfusion_id)
	Score = score.objects.filter(reperfusion= Reperfusion)
	Tiempo = tiempo.objects.filter(reperfusion= Reperfusion)
	Antecedentes = antecedentes.objects.filter(reperfusion= Reperfusion)




	if (reperfusion_id != None):
		request.session["reperfusion"] = reperfusion_id



	if request.method =='POST':
		Score = Score[0]
		Tiempo = Tiempo[0]
		Antecedentes = Antecedentes[0]
	 	form_r = reperfusionForm(request.POST, instance = Reperfusion)
		form_s = scoreForm(request.POST, instance = Score)
		form_t = tiempoForm(request.POST, instance = Tiempo)
		form_a = antecedentesForm(request.POST, instance = Antecedentes)

	 	if form_r.is_valid() and form_s.is_valid() and form_t.is_valid() and form_a.is_valid():
	 		#protocol = form.cleaned_data.get('Protocol')
	 		form_r.save()
			form_s.save()
			form_t.save()
			form_a.save()
	 		reperfusion_id = request.session["reperfusion"]


	 		#study = Study.objects.last()
	 		#messages.add_message(request, messages.SUCCESS, ('La hora de %(tipo)s para %(paciente)s se ha generado de forma exitosa.') % {'tipo':tipo, 'paciente':paciente})
			#messages.error(request, 'We could not process your request at this time.')
	 		return redirect('detalle_reperfusion', reperfusion_id)
	else:

		reperfusion_id = request.session["reperfusion"]

		Score = Score[0]
		Tiempo = Tiempo[0]
		Antecedentes = Antecedentes[0]

	 	form_r = reperfusionForm(instance = Reperfusion)
		form_s = scoreForm(instance = Score)
		form_t = tiempoForm(instance = Tiempo)
		form_a = antecedentesForm(instance = Antecedentes)
	 	#protocol_id = request.session["protocol"]
	 	#protocol=Protocol.objects.get(id= protocol_id)
	 	#form.fields['Protocol'].initial = protocol
	#protocol_id = request.session["protocol"]
	#protocol=Protocol.objects.get(id= protocol_id)
	reperfusion_id = request.session["reperfusion"]
	Reperfusion = get_object_or_404(reperfusion,pk =reperfusion_id)
	Paciente = Reperfusion.evento.paciente




	return render (request, "reperfusion/editar_reperfusion.html", { 'form_r':form_r,'form_s':form_s,'form_t':form_t,'form_a':form_a, 'Paciente': Paciente })


def detalleReperfusion (request, reperfusion_id = None):
	if (reperfusion_id != None):
		request.session["reperfusion"] = reperfusion_id

	reperfusion_id = request.session["reperfusion"]
	Reperfusion = reperfusion.objects.get(id= reperfusion_id)
	Score = score.objects.filter(reperfusion= Reperfusion)
	Tiempo = tiempo.objects.filter(reperfusion= Reperfusion)
	Antecedentes = antecedentes.objects.filter(reperfusion= Reperfusion)
	Score = Score[0]
	Tiempo = Tiempo[0]
	Antecedentes = Antecedentes[0]
	Paciente = Reperfusion.evento.paciente


	return render (request, "reperfusion/detalle_reperfusion.html", {'Reperfusion':Reperfusion,'Score':Score,'Tiempo':Tiempo,'Antecedentes':Antecedentes, 'Paciente': Paciente  })


def reportePersonal (request, reperfusion_id = None):
	if (reperfusion_id != None):
		request.session["reperfusion"] = reperfusion_id

	reperfusion_id = request.session["reperfusion"]
	Reperfusion = reperfusion.objects.get(id= reperfusion_id)
	Score = score.objects.filter(reperfusion= Reperfusion)
	Tiempo = tiempo.objects.filter(reperfusion= Reperfusion)
	Antecedentes = antecedentes.objects.filter(reperfusion= Reperfusion)
	Score = Score[0]
	Tiempo = Tiempo[0]
	Antecedentes = Antecedentes[0]
	Paciente = Reperfusion.evento.paciente

	return render (request, "reperfusion/reporte_personal.html", {'Reperfusion':Reperfusion,'Score':Score,'Tiempo':Tiempo,'Antecedentes':Antecedentes, 'Paciente': Paciente  })
