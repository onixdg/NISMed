# -*- encoding: utf-8 -*-
from django.conf.urls import url
from . import views


urlpatterns = [

url(r'^home_reperfusion/', views.homeReperfusion, name='home_reperfusion'),

url(r'^crear_reperfusion/', views.crearReperfusion , name='crear_reperfusion'),

url(r'^editar_reperfusion/(?P<reperfusion_id>[-\w]+)/$', views.editarReperfusion , name='editar_reperfusion'),

url(r'^detalle_reperfusion/(?P<reperfusion_id>[-\w]+)/$', views.detalleReperfusion , name='detalle_reperfusion'),

url(r'^reporte_personal/(?P<reperfusion_id>[-\w]+)/$', views.reportePersonal, name='reporte_personal'),
# Template que muestra la lista de espermiogramas creados, mas adelante cambia por: listar_examenes




]
