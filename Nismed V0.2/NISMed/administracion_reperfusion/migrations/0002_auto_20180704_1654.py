# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-07-04 20:54
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('administracion_usuarios', '0001_initial'),
        ('administracion_reperfusion', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='reperfusion',
            name='medico',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='administracion_usuarios.medico'),
        ),
        migrations.AddField(
            model_name='antecedentes',
            name='reperfusion',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='administracion_reperfusion.reperfusion'),
        ),
    ]
