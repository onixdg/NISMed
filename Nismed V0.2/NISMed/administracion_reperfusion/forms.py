# -*- coding: utf-8 -*-
from django import forms
from .models import *
from django.core.exceptions import ValidationError


class DateInput(forms.DateInput):
    input_type = 'date'

class reperfusionForm (forms.ModelForm):
	class Meta:
		model = reperfusion
		# Se hará petición de todos los campos del modelo, incluida claves foraneas
		fields = '__all__'
		widgets = {
            'fecha': DateInput(),

        }

class scoreForm (forms.ModelForm):
	class Meta:
		model = score
		# Se hará petición de todos los campos del modelo, incluida claves foraneas
		fields = '__all__'

class tiempoForm (forms.ModelForm):
	class Meta:
		model = tiempo
		# Se hará petición de todos los campos del modelo, incluida claves foraneas
		fields = '__all__'

class antecedentesForm (forms.ModelForm):
	class Meta:
		model = antecedentes
		# Se hará petición de todos los campos del modelo, incluida claves foraneas
		fields = '__all__'
