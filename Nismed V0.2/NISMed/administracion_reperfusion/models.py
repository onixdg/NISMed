# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from administracion_pacientes.models import *
from administracion_usuarios.models import *
from administracion_turnos.models import *

#from django.utils import timezon

# Create your models here.
class reperfusion(models.Model):
	#claves foraneas----------------------------------------------------------------------------------------------------------
	evento = models.ForeignKey(evento,blank = True,null = True)
	medico = models.ForeignKey(medico,blank = True,null = True)




	# creador= models.ManyToManyField(usuario, blank = True, null = True)
	#Lista de selección--------------------------------------------------------------------------------------------------------
	PROCEDIMIENTO = (
		('trombolisis', 'Trombolisis'),
		('trombectomia', 'Trombectomia'),
		('trombolisis-trombectomia', 'Trombolisis y Trombectomia'),
		)

	APSPECTS = (
		(0,0),
		(1,1),
		(2,2),
		(3,3),
		(4,4),
		(5,5),
		(6,6),
		(7,7),
		(8,8),
		(9,9),
		(10,10),

		)

	TICI = (
		('0', '0'),
		('1', '1'),
		('2A', '2A'),
		('2B', '2B'),
		('3', '3'),
		)

	OXFORDSHIR = (
		('TACI', 'TACI'),
		('LACI', 'LACI'),
		('PACI', 'PACI'),
		('POCI', 'POCI'),
		)

	TOAST = (
		('ateroesclerosis', 'Ateroesclerosis'),
		('cardioembolia', 'Cardioembolía'),
		('pequeño vaso', 'Pequeño vaso'),
		('2 o + causas', 'Indeterminado: 2 o + causas'),
		('estudio negativo', 'Indeterminado: Estudio negativo'),
		('estudio incompleto', 'Indeterminado: Estudio incompleto'),
		('otros', 'Otros'),
		)

	OCLUSIONGRANDESVASOS= (
		('no','No'),
		('aci','Sí: ACI'),
		('acm','Sí: ACM'),
		('tb','Sí: TB'),
		('acp','Sí: ACP'),
		)




	#atributos----------------------------------------------------------------------------------------------------------------
	fecha = models.DateField(blank=True,null = True) # formato DD MM AAAA
	procedimiento = models.CharField(max_length =30,choices= PROCEDIMIENTO,blank = True,null = True)

	#--------------------------------------------------------------------------------------------------------------------------------farmaco trombolisis
	farmacoTrombolisis = models.CharField(max_length =30,default='blabla',blank = True,null = True)


	#----------------------------------------------------------------------------------------------------------------------------------Imagenología
	aspects = models.IntegerField(choices = APSPECTS,blank = True,null = True)
	tici = models.CharField(max_length =3,choices=TICI,blank = True,null = True)
	oclusionGrandesVasos= models.CharField(max_length =4,choices=OCLUSIONGRANDESVASOS,blank = True,null = True)


	#--------------------------------------------------------------------------------------------------------------------------------------- clasificacion
	oxfordshir = models.CharField(max_length =5,choices= OXFORDSHIR,blank = True,null = True)
	toast = models.CharField(max_length =30,choices=TOAST,blank = True,null = True)


	complicaciones = models.TextField(blank = True,null = True)
	otros = models.TextField(blank = True,null = True)

class score(models.Model):
	#claves foraneas----------------------------------------------------------------------------------------------------------
	reperfusion = models.ForeignKey(reperfusion,blank = True,null = True)


	NIHSS = (
		(0,'0'),
		(1,'1'),
		(2,'2'),
		(3,'3'),
		(4,'4'),
		(5,'5'),
		(6,'6'),
		(7,'7'),
		(8,'8'),
		(9,'9'),
		(10,'10'),
		(11,'11'),
		(12,'12'),
		(13,'13'),
		(14,'14'),
		(15,'15'),
		(16,'16'),
		(17,'17'),
		(18,'18'),
		(19,'19'),
		(20,'20'),
		(21,'21'),
		(22,'22'),
		(23,'23'),
		(24,'24'),
		(25,'25'),
		(26,'26'),
		(27,'27'),
		(28,'28'),
		(29,'29'),
		(30,'30'),
		(31,'31'),
		(32,'32'),
		(33,'33'),
		(34,'34'),
		(35,'35'),
		(36,'36'),
		(37,'37'),
		(38,'38'),
		(39,'39'),
		(40,'40'),
		(41,'41'),
		(42,'42'),
		(43,'43'),
		(44,'44'),
		(45,'45'),
				)

	mRS = (
		(0,'0'),
		(1,'1'),
		(2,'2'),
		(3,'3'),
		(4,'4'),
		(5,'5'),
		(6,'6'),
			)


	#---------------------------------------------------------------------------------------------------------------------------------Score
	NIHSSScaleIngreso = models.IntegerField(choices = NIHSS,blank = True,null = True)
	NIHSSScale24hrs = models.IntegerField(choices = NIHSS,blank = True,null = True)
	NIHSSScaleAlta = models.IntegerField(choices = NIHSS,blank = True,null = True)
	NIHSSScale3Meses = models.IntegerField(choices = NIHSS,blank = True,null = True)
	mRSScaleIngreso = models.IntegerField(choices = mRS,blank = True,null = True)
	mRSScaleAlta = models.IntegerField(choices = mRS,blank = True,null = True)
	mRSScale3Meses = models.IntegerField(choices = mRS,blank = True,null = True)


class tiempo(models.Model):
	#claves foraneas----------------------------------------------------------------------------------------------------------
	reperfusion = models.ForeignKey(reperfusion,blank = True,null = True)
	#----------------------------------------------------------------------------------------------------------------------------------tiempos criticos
	horaInicioSintomas = models.TimeField(blank=True,null = True)
	horaLlegadaUEA = models.TimeField(blank=True,null = True)
	horaTRIAGE = models.TimeField(blank=True,null = True)
	horaEvaluacionNeurologia= models.TimeField(blank=True,null = True)
	horaTCEncefalo= models.TimeField(blank=True,null = True)
	horaBoloTrombolisis= models.TimeField(blank=True,null = True)
	horaTrombectomia= models.TimeField(blank=True,null = True)
	horaReperfusion= models.TimeField(blank=True,null = True)

class antecedentes(models.Model):
	#claves foraneas----------------------------------------------------------------------------------------------------------
	reperfusion = models.ForeignKey(reperfusion,blank = True,null = True)

	#Lista de selección--------------------------------------------------------------------------------------------------------

	HTA = (
		('si','Sí'),
		('no','No'),
		)
	DM = (
		('si','Sí'),
		('no','No'),
		)
	DLP= (
		('si','Sí'),
		('no','No'),
		)
	TBQ = (
		('si','Sí'),
		('no','No'),
		)
	ACxFA = (
		('si','Sí'),
		('no','No'),
		)
	ANTICOAGULANTE = (
		('si','Sí'),
		('no','No'),
		)
	ACVPREVIO = (
		('si','Sí'),
		('no','No'),
		)

	#atributos----------------------------------------------------------------------------------------------------------------
	hta = models.CharField(max_length =3,choices= HTA,blank = True,null = True) # formato DD MM AAAA
	dm = models.CharField(max_length =3,choices= DM,blank = True,null = True) # formato DD MM AAAA
	dlp = models.CharField(max_length =3,choices= DLP,blank = True,null = True) # formato DD MM AAAA
	tbq = models.CharField(max_length =3,choices= TBQ,blank = True,null = True) # formato DD MM AAAA
	acxfa = models.CharField(max_length =3,choices= ACxFA,blank = True,null = True) # formato DD MM AAAA
	farmacos = models.CharField(max_length =20,blank = True,null = True)
	anticoagulante =  models.CharField(max_length =3,choices= ANTICOAGULANTE,blank = True,null = True)
	acvPrevio =  models.CharField(max_length =3,choices= ACVPREVIO,blank = True,null = True)
	antecedentesMedicos =  models.TextField(blank = True,null = True)                   # textfield?
	otros = models.TextField(blank = True,null = True)
