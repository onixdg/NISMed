# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class paciente(models.Model):
	#Lista de selección--------------------------------------------------------------------------------------------------------
	ESTADO = (
		('vivo','vivo'),
		('fallecido','fallecido'),

	)
	SEXO = (
		(1,'hombre'),
		(2,'mujer'),
		(3,'indeterminado'),
		(9,'desconocido'),
		)

	INTRUCCION = (
		(1,'Pre-básica'),
		(2,'Básica'),
		(3,'Media'),
		(4,'Técnica de nivel superior'),
		(5,'Superior'),
		(6,'Ninguno'),
		)
	#atributos-----------------------------------------------------------------------------------------------------------------
	nFicha= models.IntegerField(blank=True,null = True)
	nombres = models.CharField(max_length = 50)
	primerApellido = models.CharField("Apellido paterno",max_length = 30)
	segundoApellido = models.CharField("Apellido materno",max_length = 30,blank=True,null = True)
	RUN = models.IntegerField("RUT",blank=True,null = True )
	DV = models.CharField(max_length=1,blank=True,null = True) # En caso de ser extranjero debería detallarse DI
	sexo = models.IntegerField(choices=SEXO,blank=True,null = True)
	fechaNacimiento = models.DateField(blank=True,null = True) # formato DD MM AAAA
	email = models.EmailField(max_length=50,blank=True,null = True)
	telefono = models.IntegerField(blank=True,null = True)
	#Region = models.CharField("Región",max_length = 2,choices=REGION,blank=True,null = True)
	#Provincia = models.CharField(max_length = 3,choices=PROVINCIA,blank=True,null = True)
	#Comuna = models.CharField(max_length = 5,choices=COMUNA,blank=True,null = True)
	direccion = models.CharField(max_length=100,blank=True,null = True)
	ciudad = models.CharField(max_length=30,blank=True,null = True)
	nivelInstruccion = models.IntegerField(blank=True,null = True, choices = INTRUCCION)
	cuentaCorriente =models.CharField(max_length=30)
	#Origen = models.CharField(max_length = 1,choices = ORIGEN,blank=True,null = True)
	#Nacionalidad = models.IntegerField(choices=NACIONALIDAD,blank=True,null = True)
	DI = models.CharField(max_length = 20,blank=True,null = True)
	estado = models.CharField(max_length = 10, blank=True, null =True)



class evento(models.Model):
		#claves foraneas----------------------------------------------------------------------------------------------------------
		paciente= models.ForeignKey(paciente,blank = True,null = True)

		#Lista de selección--------------------------------------------------------------------------------------------------------
		ORIGEN = (
			('turno','Turno'),
			('sala','Sala'),
			('reperfusion','Reperfusion'),
		)

		TIPO = (
			('ingreso','Ingreso'),
			('trasferencia','Trasferencia'),
			('alta','Alta'),
			('muerte','Muerte'),
		)

		#atributos-----------------------------------------------------------------------------------------------------------------

		fecha = models.DateField(blank=True,null = True) # formato DD MM AAAA
		origen = models.CharField(max_length = 15,choices = ORIGEN,blank=True,null = True)
		tipo = models.CharField(max_length = 15,choices = TIPO,blank=True,null = True)
