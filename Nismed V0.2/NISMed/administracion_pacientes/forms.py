# -*- coding: utf-8 -*-
from django import forms
from .models import *
from django.core.exceptions import ValidationError


class DateInput(forms.DateInput):
    input_type = 'date'

class pacienteForm (forms.ModelForm):

    class Meta:

		model = paciente
		# Se hará petición de todos los campos del modelo, incluida claves foraneas
		fields = '__all__'
		widgets = {
            'fechaNacimiento': DateInput(),
            'RUN' : forms.TextInput(attrs={'size':8}),
            'DV' : forms.TextInput(attrs={'size':1}),
            'Codigo' : forms.TextInput(attrs={'size':3}),
            'Email' : forms.EmailInput(),
        }


    def clean_DV(self):
        diccionario_limpio = self.cleaned_data
        DV = diccionario_limpio.get("DV")
        RUN = diccionario_limpio.get("RUN")
        if (RUN and (DV == None or (len(str(DV))==0))) :
            raise forms.ValidationError("El dígito verificador es requerido")
        return DV

    def clean_RUN(self):
        diccionario_limpio = self.cleaned_data
        RUN = diccionario_limpio.get("RUN")
        validacion = paciente.objects.filter(RUN = RUN)
        if(RUN):
            if self.instance.pk is not None:
                if self.instance.RUN != RUN:
                    if (validacion) :
                        raise forms.ValidationError("El rut ya existe en el sistema " )
            else:
                if (validacion) :
                    raise forms.ValidationError("El rut ya existe en el sistema "  )
        return RUN

	def clean_DI(self):
		diccionario_limpio = self.cleaned_data
		DI = diccionario_limpio.get("DI")

		RUN = diccionario_limpio.get("RUN")
		if ((DI == None or (len(str(DI))==0)) and (RUN == None or (len(str(RUN))==0))) :
				raise forms.ValidationError("Se necesita especificar RUN o alguna otra identificación nacional")
		else:
			if (DI and RUN):
				raise forms.ValidationError("Especifique una sola forma de identificación entre RUN o DI " )
		return DI

class pacienteRapForm (forms.ModelForm):

    class Meta:

		model = paciente
		# Se hará petición de todos los campos del modelo, incluida claves foraneas
		fields = '__all__'
