# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
#from administracion_turnos.models import *
#from administracion_usuarios.models import medico
from administracion_pacientes.models import *
from django.http import HttpResponse
from forms import *
from django.shortcuts import get_object_or_404

# Create your views here.
def crearPaciente(request):
 	#if (paciente_id == None):
 		#return redirect('seleccionar_paciente')
 	#else:

	 	if request.method =='POST':
	 		form = pacienteForm(request.POST)
	 		if form.is_valid():
	 			form.save()


	 			#messages.add_message(request, messages.SUCCESS, ('La hora de %(tipo)s para %(paciente)s se ha generado de forma exitosa.') % {'tipo':tipo, 'paciente':paciente})

	 			Paciente =paciente.objects.last()
	 			#return redirect('ficha_paciente',paciente.id)
				return redirect('home_turno')
	 	else:

	 		form= pacienteForm()
	 	return render (request, 'paciente/crear_paciente.html',{'form':form})


def crearPacienteTurnoRap(request):


   	 	if request.method =='POST':
   	 		form = pacienteRapForm(request.POST)
   	 		if form.is_valid():
   	 			form.save()


   	 			#messages.add_message(request, messages.SUCCESS, ('La hora de %(tipo)s para %(paciente)s se ha generado de forma exitosa.') % {'tipo':tipo, 'paciente':paciente})

   	 			Paciente =paciente.objects.last()
   	 			#return redirect('ficha_paciente',paciente.id)
   				return redirect('crear_hospitalizacion')
   	 	else:

   	 		form= pacienteRapForm()
   	 	return render (request, 'paciente/crear_paciente_tur_rap.html',{'form':form})






def pacienteIncompletoT(request):

	return render (request, 'paciente/crear_paciente_tur_rap.html')
