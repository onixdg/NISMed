var Fn = {
    // Valida el rut con su cadena completa "XXXXXXXX-X"
    validaRut : function (rutCompleto) {
        if (!/^[0-9]+-[0-9kK]{1}$/.test( rutCompleto ))
            return false;
        var tmp     = rutCompleto.split('-');
        var digv    = tmp[1];
        var rut     = tmp[0];
        if ( digv == 'K' ) digv = 'k' ;
        return (Fn.dv(rut) == digv );
    },
    dv : function(T){
        var M=0,S=1;
        for(;T;T=Math.floor(T/10))
            S=(S+T%10*(9-M++%6))%11;
        return S?S-1:'k';
    }
}

function validar_rut(){
    var rut = document.getElementById("rut").value;
    var dv = document.getElementById("dv").value;
    if (rut != "" && dv != ""){
        var rutCompleto =rut +"-"+dv
        if (Fn.validaRut(rutCompleto)){
            $("#msgerror").html("El rut es válido");
        } else {
            $("#msgerror").html("El Rut no es válido");
            document.getElementById("rut").value = "";
            document.getElementById("dv").value ="";
        }
    }

}
