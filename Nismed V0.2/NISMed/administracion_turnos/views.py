# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from administracion_turnos.models import *
from administracion_usuarios.models import medico
from administracion_pacientes.models import *
from django.http import HttpResponse
from forms import *
from administracion_pacientes.forms import *
from django.shortcuts import get_object_or_404
from datetime import *
from datetime import date
from datetime import timedelta
from administracion_informes.views import *
from django.utils import timezone


def turnoCerrado(request):







	return render (request, 'turno/turno_cerrado.html')


#HOME------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------HOME TURNO
def homeTurno (request):

	Turno = turno.objects.filter(estado = 'activo', unidad ='turno')
	Hospitalizacion = hospitalizacionTurno.objects.filter( estado = 'vigente').order_by('fechaInicio')


	#Control = control.objects.filter(hospitalizacion= Hospitalizacion)
	#ControlI = Control[1]Turno
	#ControlF = Control[-1]
	#fI = ControlI.fecha
	hoy = date.today()
	#dias =  (fF - fI).days
	ahora = datetime.now()







	return render (request, 'turno/home_turno.html', {'Turno':Turno, 'Hospitalizacion':Hospitalizacion, 'hoy': hoy, 'ahora': ahora})



#TURNO-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------CREAR Y CERRAR TURNO
def crearTurno(request):
	Turno = turno.objects.last()

	if ( Turno and Turno.estado == 'activo'):
		return HttpResponse('<h1>Lo sentimos, no se puede crear un nuevo turno debido a que no se ha cerrado el turno anterior</h1>')
	else:
		name = request.user.username
		Medico = medico.objects.get (username = name)
		if (Medico.tipo == 'tur-staff'):

		 	if request.method =='POST':
		 		form = turnoForm(request.POST)
		 		if form.is_valid():
		 			form.save()


		 			#messages.add_message(request, messages.SUCCESS, ('La hora de %(tipo)s para %(paciente)s se ha generado de forma exitosa.') % {'tipo':tipo, 'paciente':paciente})

		 			Turno =turno.objects.last()
		 			return redirect('home_turno')



		 	else:

		 		form= turnoForm()

			#hospitalizacion_id =request.session["hospitalizacion"]
			#Hospitalizacion= hospitalizacion.objects.get(id= hospitalizacion_id)

			form.fields['estado'].initial = 'activo'
			form.fields['unidad'].initial = 'turno'
			form.fields['medico'].initial = Medico
		 	return render (request, 'turno/crear_turno.html',{'form':form})
		else:
			return HttpResponse('<h1>Lo sentimos, no cuenta con los permisos necesarios para crear turnos en el sistema</h1>')


def cerrarTurno(request):
	name =request.user.username
	Medico = medico.objects.get(username = name )
	Turno = turno.objects.last()
	responsible = Turno.medico.all()
	nombre = "nada"
	for r in responsible:

		if (r.tipo == 'tur-staff'):
			nombre = r.username
	print Medico.username


	if (Medico.username != nombre):

		return HttpResponse('<h1>Lo sentimos, no cuenta con los permisos necesarios para cerrar el turno</h1>')
	else :

		Turno.estado ="cerrado"
		Turno.fechaTermino = datetime.now()
		turno_id = Turno.id
		Turno.save()





	 	return redirect('turno_cerrado')

#REVISAR PLANES Y PENDIENTES------------------------------------------------------------------------------------------------------------------------------------------------------REVISAR PLANES Y PENDIENTES

def revisarPlanes(request):
	name =request.user.username
	Medico = medico.objects.get(username = name )
	Turno = turno.objects.last()
	responsible = Turno.medico.all()
	nombre = "nada"
	for r in responsible:

		if (r.tipo == 'tur-staff'):
			nombre = r.username
	print Medico.username


	if (Medico.username != nombre):
		return HttpResponse('<h1>Lo sentimos, no cuenta con los permisos necesarios para cerrar el turno</h1>')
	else :
		Turno = turno.objects.filter( estado = 'activo' )
		Hospitalizacion = hospitalizacionTurno.objects.filter( estado = 'vigente').order_by('fechaInicio')
		hoy = date.today()

		ahora = datetime.now()

		ok = 'si'
		for h in Hospitalizacion:

			Control = control.objects.filter (hospitalizacion = h).last()


			if (Control.planesPendiente == " " or Control.planesPendiente == None or len(Control.planesPendiente) == 0 ):

				ok = 'no'


	 	return render (request, "turno/revisar_planes.html", {'Hospitalizacion': Hospitalizacion, 'Turno' : Turno, 'hoy':hoy, 'ok':ok })


def crearPlanes (request, control_id = None):
	Control = get_object_or_404(control,pk =control_id)
	if (control_id):
		request.session["control"] = control_id

	if request.method =='POST':
		form = controlForm(request.POST, instance = Control )
		if  form.is_valid() :
			form.save()




	 			#messages.add_message(request, messages.SUCCESS, ('La hora de %(tipo)s para %(paciente)s se ha generado de forma exitosa.') % {'tipo':tipo, 'paciente':paciente})

			return HttpResponse('<h1>Plan y pendiente registrado, actualice página de planes y pendientes para visualizar cambios</h1>')
	else:

		form = controlForm(instance = Control)

	return render (request, 'turno/crear_planes.html',{'form':form, 'Control':Control})




#HOSPITALIZACION-------------------------------------------------------------------------------------------------------------------------------------------------------------------CREAR HOSPITALIZACION



def crearHospitalizacion (request):
 	#if (paciente_id == None):
 		#return redirect('seleccionar_paciente')
 	#else:
	if request.method =='POST':

		form_p = pacienteRapForm(request.POST)
		form_c = controlForm(request.POST)
		form = hospitalizacionForm(request.POST)
		if  form_p.is_valid() and form_c.is_valid():
			form_p.save()
			Paciente = paciente.objects.last()
			Evento = evento.objects.create(fecha = date.today() , origen = 'turno', tipo = 'ingreso', paciente = Paciente)



			form.save()
			form_c.save()
			Control = control.objects.last()
			Hospitalizacion = hospitalizacionTurno.objects.last()
			Hospitalizacion.evento = Evento
			Control.hospitalizacion = Hospitalizacion
			Control.save()
			Hospitalizacion.save()



	 			#messages.add_message(request, messages.SUCCESS, ('La hora de %(tipo)s para %(paciente)s se ha generado de forma exitosa.') % {'tipo':tipo, 'paciente':paciente})

			return redirect('home_turno')
	else:

		form_p = pacienteRapForm()
		form = hospitalizacionForm()
		form_c = controlForm()


	Turno = turno.objects.filter(estado = 'activo', unidad ='turno')
	form.fields['turno'].initial = Turno[0]
	form.fields['fechaInicio'].initial = date.today()

	form_c.fields['fecha'].initial = date.today()
	form_c.fields['primer'].initial = 'si'




	return render (request, 'turno/crear_hospitalizacion.html',{'form_p':form_p, 'form':form, 'form_c':form_c})



#CONTROL-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------CREAR CONTROL




def crearControl (request, hospitalizacion_id = None):
	if (hospitalizacion_id != None):
		request.session["hospitalizacion"] = hospitalizacion_id

	if request.method =='POST':


		form = controlForm(request.POST)
		if  form.is_valid() :
			form.save()




	 			#messages.add_message(request, messages.SUCCESS, ('La hora de %(tipo)s para %(paciente)s se ha generado de forma exitosa.') % {'tipo':tipo, 'paciente':paciente})

			return redirect('home_turno')
	else:

		form = controlForm()


	form.fields['fecha'].initial = date.today()
	form.fields['primer'].initial = 'no'
	hospitalizacion_id = request.session["hospitalizacion"]
	Hospitalizacion = hospitalizacionTurno.objects.get(id= hospitalizacion_id)
	Control = control.objects.filter(hospitalizacion= Hospitalizacion).last()
	form.fields['hospitalizacion'].initial = Hospitalizacion
	form.fields['ubicacion'].initial = Control.ubicacion
	form.fields['cama'].initial = Control.cama
	form.fields['antecedentesHistoria'].initial = Control.antecedentesHistoria
	form.fields['examenFisico'].initial = Control.examenFisico
	form.fields['labImagenes'].initial = Control.labImagenes
	form.fields['diagnostico'].initial = Control.diagnostico
	form.fields['planesPendiente'].initial = Control.planesPendiente
	form.fields['rankinScale'].initial = Control.rankinScale
	form.fields['glasgowScale'].initial = Control.glasgowScale
	form.fields['nihssScale'].initial = Control.nihssScale




	return render (request, 'turno/crear_control.html',{'form':form})


def listarControl (request, hospitalizacion_id = None):
	if (hospitalizacion_id != None):
		request.session["hospitalizacion"] = hospitalizacion_id
	hospitalizacion_id = request.session["hospitalizacion"]
	Hospitalizacion = hospitalizacionTurno.objects.get(id= hospitalizacion_id)
	Control = control.objects.filter(hospitalizacion= Hospitalizacion)


	return render (request, "turno/listar_control.html", {'Hospitalizacion': Hospitalizacion, 'Control':Control })



def detalleControl (request, control_id = None):
	if (control_id != None):
		request.session["control"] = control_id
	control_id = request.session["control"]
	Control = control.objects.get(id= control_id)



	return render (request, "turno/detalle_control.html", {'Control':Control })




def editarControl (request, control_id = None):
	Control = get_object_or_404(control,pk =control_id)
	if (control_id != None):
		request.session["control"] = control_id
		request.session["hospitalizacion"] = Control.hospitalizacion.id


	if request.method =='POST':
	 	form = controlForm(request.POST, instance = Control)
	 	if form.is_valid():
	 		#protocol = form.cleaned_data.get('Protocol')
	 		form.save()
	 		hospitalizacion_id = request.session["hospitalizacion"]


	 		#study = Study.objects.last()
	 		#messages.add_message(request, messages.SUCCESS, ('La hora de %(tipo)s para %(paciente)s se ha generado de forma exitosa.') % {'tipo':tipo, 'paciente':paciente})
			#messages.error(request, 'We could not process your request at this time.')
	 		return redirect('listar_control', hospitalizacion_id)
	else:

	 	form= controlForm(instance = Control)
	 	#protocol_id = request.session["protocol"]
	 	#protocol=Protocol.objects.get(id= protocol_id)
	 	#form.fields['Protocol'].initial = protocol
	#protocol_id = request.session["protocol"]
	#protocol=Protocol.objects.get(id= protocol_id)
	control_id = request.session["control"]
	Control = get_object_or_404(control,pk =control_id)




	return render (request, "turno/editar_control.html", { 'form':form, 'Control': Control })




#REVISION--------------------------------------------------------------------------------------------------------------------------------------------------------------------------CREAR REVISION

def crearRevision (request, control_id = None):
	if (control_id != None):
		request.session["control"] = control_id

	if request.method =='POST':


		form = revisionForm(request.POST)
		if  form.is_valid() :
			estado = form.cleaned_data.get('revision')

			hoy =  datetime.now()

			form.save()
			Revision = revision.objects.last()


			if (estado == "SOS"):
				 Revision.fecha = hoy + timedelta(minutes=15)
			elif (estado == "hoy"):
				Revision.fecha = hoy
			elif (estado == "24hrs"):
				Revision.fecha = hoy + timedelta(days=1)
			elif (estado == "48hrs"):
				Revision.fecha = hoy + timedelta(days=2)

			Revision.save()





	 			#messages.add_message(request, messages.SUCCESS, ('La hora de %(tipo)s para %(paciente)s se ha generado de forma exitosa.') % {'tipo':tipo, 'paciente':paciente})

			return HttpResponse("Estado guardado")
	else:

		form = revisionForm()



	control_id = request.session["control"]
	Control = control.objects.get(id= control_id)

	form.fields['control'].initial = Control





	return render (request, 'turno/crear_revision.html',{'form':form})










#REVISION--------------------------------------------------------------------------------------------------------------------------------------------------------------------------CREAR REVISION

def crearResumen (request, turno_id = None):
	if (turno_id != None):
		request.session["turno"] = turno_id

	if request.method =='POST':


		form = resumenForm(request.POST)
		if  form.is_valid() :

			form.save()


	 			#messages.add_message(request, messages.SUCCESS, ('La hora de %(tipo)s para %(paciente)s se ha generado de forma exitosa.') % {'tipo':tipo, 'paciente':paciente})

			return redirect('cerrar_turno')
	else:

		form = resumenForm()



	turno_id = request.session["turno"]
	Turno = turno.objects.get(id= turno_id)

	form.fields['turno'].initial = Turno





	return render (request, 'turno/crear_resumen.html',{'form':form})
