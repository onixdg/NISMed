# -*- encoding: utf-8 -*-
from django.conf.urls import url
from . import views

urlpatterns = [
url(r'^crear_turno/', views.crearTurno, name='crear_turno'),



url(r'^cerrar_turno/', views.cerrarTurno, name='cerrar_turno'),

url(r'^home_turno/', views.homeTurno, name='home_turno'),

url(r'^turno_cerrado/', views.turnoCerrado, name='turno_cerrado'),

url(r'^revisar_planes/', views.revisarPlanes, name='revisar_planes'),

url(r'^crear_planes/(?P<control_id>[-\w]+)/$', views.crearPlanes , name='crear_planes'),
url(r'^crear_planes/', views.crearPlanes, name='crear_planes'),

url(r'^crear_resumen/(?P<turno_id>[-\w]+)/$', views.crearResumen , name='crear_resumen'),
url(r'^crear_resumen/', views.crearResumen , name='crear_resumen'),


url(r'^crear_control/(?P<hospitalizacion_id>[-\w]+)/$', views.crearControl, name='crear_control'),
url(r'^crear_control/', views.crearControl, name='crear_control'),

url(r'^crear_revision/(?P<control_id>[-\w]+)/$', views.crearRevision, name='crear_revision'),
url(r'^crear_revision/', views.crearRevision, name='crear_revision'),

url(r'^listar_control/(?P<hospitalizacion_id>[-\w]+)/$', views.listarControl, name='listar_control'),
url(r'^listar_control/', views.listarControl, name='listar_control'),

url(r'^detalle_control/(?P<control_id>[-\w]+)/$', views.detalleControl, name='detalle_control'),
url(r'^detalle_control/', views.detalleControl, name='detalle_control'),

url(r'^editar_control/(?P<control_id>[-\w]+)/$', views.editarControl, name='editar_control'),
url(r'^editar_control/', views.editarControl, name='editar_control'),


url(r'^crear_hospitalizacion/', views.crearHospitalizacion, name='crear_hospitalizacion'),

]
