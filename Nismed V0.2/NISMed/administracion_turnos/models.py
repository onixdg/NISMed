# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from administracion_pacientes.models import *
from administracion_usuarios.models import *
#from django.utils import timezon

# Create your models here.






class turno(models.Model):
	#claves foraneas----------------------------------------------------------------------------------------------------------
	medico= models.ManyToManyField(medico,blank = True,null = True)
	# creador= models.ManyToManyField(usuario, blank = True, null = True)
	#Lista de selección--------------------------------------------------------------------------------------------------------
	ESTADO = (
		('activo','Activo'),
		('cerrado','Cerrado'),
		('desfasado','Desfasado'),
		)
	UNIDAD = (
		('turno', 'Turno'),
		('sala', 'Sala'),

	)

	#atributos----------------------------------------------------------------------------------------------------------------
	fechaInicio = models.DateField(blank=True,null = True) # formato DD MM AAAA
	fechaTermino =models.DateField(blank=True,null = True)
	estado = models.CharField(max_length =20,choices=ESTADO,default = 'activo')
	unidad = models.CharField(max_length =20,choices=UNIDAD)



#class turnoMedico(models.Model):
	#claves foraneas----------------------------------------------------------------------------------------------------------
	#medico= models.ForeignKey(medico,blank = True,null = True)
	#turno= models.ForeignKey(turno,blank = True,null = True)

	#Lista de selección--------------------------------------------------------------------------------------------------------


	#atributos----------------------------------------------------------------------------------------------------------------

	#rol = (max_length = 2,choices=ESTADO)



class resumen(models.Model):
	#claves foraneas----------------------------------------------------------------------------------------------------------
	turno= models.ForeignKey(turno,blank = True,null = True)


	#atributos----------------------------------------------------------------------------------------------------------------
	ingresosSala =  models.IntegerField()
	clavesStroke = models.IntegerField()
	trombolisis  = models.IntegerField()
	trombectomias  = models.IntegerField()
	interconsultasUEA = models.IntegerField()
	interconsultasOtroServicios = models.IntegerField()
	controles = models.IntegerField()
	controlesMCM = models.IntegerField()
	altasSala = models.IntegerField()
	observacion = models.TextField()



#pensar si sacar estado a una entidad sola por temas de historial del estado y traspaso

class hospitalizacionTurno(models.Model):
	#claves foraneas----------------------------------------------------------------------------------------------------------
	turno= models.ForeignKey(turno,blank = True,null = True)
	evento = models.ForeignKey(evento,blank = True,null = True )
	#Lista de selección--------------------------------------------------------------------------------------------------------

		#MEJORAR ESTADO
	ESTADO = (
		('vigente','Vigente'),
		('finalizado','Finalizado'),

		)
	#claves foraneas----------------------------------------------------------------------------------------------------------


	fechaInicio = models.DateField(blank=True,null = True) # formato DD MM AAAA
	fechaTermino =models.DateField(blank=True,null = True) # formato DD MM AAAA
	estado = models.CharField(max_length = 20,choices=ESTADO,blank=True,null = True, default = 'vigente')




class control(models.Model):
	#claves foraneas----------------------------------------------------------------------------------------------------------
	hospitalizacion= models.ForeignKey(hospitalizacionTurno,blank = True,null = True)
	#Lista de selección--------------------------------------------------------------------------------------------------------
	#MEJORAR ESTADO

	UBICACION = (
			('UMA','Activo'),
			('transferido','trasferico'),
			('alta','Alta'),
			)

	ESTADO = (
			('vigente','Vigente'),
			('transferido','Transferido'),
			('alta','alta'),
			)

	RANKIN = (
		(0,'0'),
		(1,'1'),
		(2,'2'),
		(3,'3'),
		(4,'4'),
		(5,'5'),
		(6,'6'),
			)
	GLASGOW = (
		(3,'3'),
		(4,'4'),
		(5,'5'),
		(6,'6'),
		(7,'7'),
		(8,'8'),
		(9,'9'),
		(10,'10'),
		(11,'11'),
		(12,'12'),
		(13,'13'),
		(14,'14'),
		(15,'15'),
				)
	NIHSS = (
		(0,'0'),
		(1,'1'),
		(2,'2'),
		(3,'3'),
		(4,'4'),
		(5,'5'),
		(6,'6'),
		(7,'7'),
		(8,'8'),
		(9,'9'),
		(10,'10'),
		(11,'11'),
		(12,'12'),
		(13,'13'),
		(14,'14'),
		(15,'15'),
		(16,'16'),
		(17,'17'),
		(18,'18'),
		(19,'19'),
		(20,'20'),
		(21,'21'),
		(22,'22'),
		(23,'23'),
		(24,'24'),
		(25,'25'),
		(26,'26'),
		(27,'27'),
		(28,'28'),
		(29,'29'),
		(30,'30'),
		(31,'31'),
		(32,'32'),
		(33,'33'),
		(34,'34'),
		(35,'35'),
		(36,'36'),
		(37,'37'),
		(38,'38'),
		(39,'39'),
		(40,'40'),
		(41,'41'),
		(42,'42'),
		(43,'43'),
		(44,'44'),
		(45,'45'),

				)



	#atributos-----------------------------------------------------------------------------------------------------------------
	primer =models.CharField(max_length = 3, default= 'no')
	ubicacion= models.CharField(max_length = 100)
	cama = models.IntegerField()

	fecha = models.DateField(blank=True,null = True)

	antecedentesHistoria =models.TextField()
	examenFisico= models.TextField()
	labImagenes = models.TextField()
	diagnostico = models.TextField()
	estado = models.CharField(max_length = 20,choices=ESTADO,blank=True,null = True, default= 'vigente')
	planesPendiente = models.TextField(blank=True,null = True)
	rankinScale = models.IntegerField(choices = RANKIN)
	glasgowScale = models.IntegerField(choices = GLASGOW)
	nihssScale = models.IntegerField(choices = NIHSS)



class revision(models.Model):
	#claves foraneas----------------------------------------------------------------------------------------------------------
	control= models.ForeignKey(control,blank = True,null = True)
	#Lista de selección--------------------------------------------------------------------------------------------------------

	REVISION = (
		('hoy','Hoy'),
		('24hrs','24 hrs'),
		('48hrs','48 hrs'),
		('SOS','SOS'),
		)
	#claves foraneas----------------------------------------------------------------------------------------------------------


	fecha = models.DateTimeField(blank=True,null = True) # formato DD MM AAAA

	revision = models.CharField(max_length = 20,choices=REVISION,blank=True,null = True)
