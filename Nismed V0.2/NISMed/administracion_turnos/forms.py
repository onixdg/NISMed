# -*- coding: utf-8 -*-
from django import forms
from .models import *
from django.core.exceptions import ValidationError


class DateInput(forms.DateInput):
    input_type = 'date'

class turnoForm (forms.ModelForm):
	class Meta:
		model = turno
		# Se hará petición de todos los campos del modelo, incluida claves foraneas
		fields = '__all__'
		widgets = {
            'fechaInicio': DateInput(),
			'fechaTermino': DateInput(),
        }

class controlForm (forms.ModelForm):
	class Meta:
		model = control
		# Se hará petición de todos los campos del modelo, incluida claves foraneas
		fields = '__all__'
		widgets = {
            'fecha': DateInput(),

        }


class hospitalizacionForm (forms.ModelForm):
	class Meta:
		model = hospitalizacionTurno
		# Se hará petición de todos los campos del modelo, incluida claves foraneas
		fields = '__all__'
        widgets = {
            'fechaInicio': DateInput(),
			'fechaTermino': DateInput(),
        }

class revisionForm (forms.ModelForm):
	class Meta:
		model = revision
		# Se hará petición de todos los campos del modelo, incluida claves foraneas
		fields = '__all__'

class resumenForm (forms.ModelForm):
	class Meta:
		model = resumen
		# Se hará petición de todos los campos del modelo, incluida claves foraneas
		fields = '__all__'
