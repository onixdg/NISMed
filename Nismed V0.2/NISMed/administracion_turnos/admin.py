# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import *

admin.site.register(turno)
admin.site.register(resumen)

admin.site.register(hospitalizacionTurno)
admin.site.register(control)
admin.site.register(revision)
